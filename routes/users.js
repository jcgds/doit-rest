var express = require('express');
var {User} = require('../models/user.js');
var {authenticate} = require('../middlewares/authenticate');

var router = express.Router();

router.route('/users')
.get((req,res) => {
  User.find({},'username nombre apellido fechaDeNacimiento fechaDeRegistro activo').then( (users) => {
    res.send({users});
  }, (e) => {
    res.status(400).send(e);
  });
});

router.route('/users/register')
.post((req,res) => {
  var newUser = new User(req.body);

  newUser.save().then(() => {
    return newUser.generateAuthToken();
  }).then((token) => {
    res.header('x-auth', token).status(201).send(newUser);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

router.route('/users/me')
.get(authenticate, (req,res) => {
  res.send(req.user);
});

module.exports = router;
