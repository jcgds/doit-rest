var express = require('express');
var {User} = require('../models/user.js');
const _ = require('lodash');
var {authenticate} = require('../middlewares/authenticate');
var {loginLimiter} = require('../middlewares/loginLimiter');
var router = express.Router();

router.use('/login', loginLimiter);

router.route('/login')
.post((req, res) => {
  var body = _.pick(req.body, ['username','password']);

  User.findByCredentials(body.username, body.password).then((user) => {
    if (user.activo === false)
      return res.status(403).send({'detail': `${user.username} is blocked.`});

    return user.generateAuthToken().then((token) => {
      //console.log("Logged in!");
      //Reseteamos la cuenta de intentos de login
      loginLimiter.resetKey(body.username);
      var result = user.toJSON();
      result["token"] = token;
      res.header('x-auth', token).status(200).send(result);
    });
  }).catch((err) => {
    res.status(400).send({'detail': 'Invalid username or password'});
  });
});

router.route('/logout')
.delete(authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send({'detail':'Logout exitoso'});
  }, () => {
    res.status(400).send({'detail':'Error borrando token'});
  });
});

module.exports = router;
