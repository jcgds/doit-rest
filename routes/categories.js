const express = require('express');
const {Category} = require('../models/category');

var router = express.Router();

router.route('/categories')
.get((req,res) => {
  Category.find({activa: true}).then( (categories) => {
    res.send({categories});
  }, (e) => {
    res.status(400).send(e);
  });
})
.post( (req,res) => {
  var category = new Category(req.body);

  category.save( (err) => {
    if (err) {
     return res.status(400).send(err);
    }
    res.status(201).send(category);
  });
});

module.exports = router;
