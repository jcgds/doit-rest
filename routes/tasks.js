const _ = require('lodash');
const {ObjectID} = require('mongodb');
var express = require('express');
var {Task} = require('../models/task');
var {User} = require('../models/user');
var {Category} = require('../models/category');
var {authenticate} = require('../middlewares/authenticate');

var router = express.Router();

router.route('/tasks')
.post( authenticate, (req,res) => {
  var newTask = new Task(req.body);
  newTask.user = req.user._id;
  console.log(newTask);

  console.log(`category: ${newTask.category}`);
  //Primero validamos que la categoria exista
  Category.findOne({_id: newTask.category}).then( (category) => {
    //Si no conseguimos la categoria, devolvemos un detail
    if (!category) {
      return res.status(400).send({"detail": `Category ${newTask.category} not found`});
    }
    //Categoria valida, guardamos la task
    newTask.save().then( () => {
      console.log("Tarea creada");
      res.status(201).send(newTask);
    }).catch((err) => {
      console.log("detail al crear tarea");
      res.status(400).send(err);
    });
  }).catch( (err) => {
    res.status(400).send(err);
  });
});

router.route('/tasks/me')
.get(authenticate, (req,res) => {
  var UserId = new ObjectID(req.user._id);
  Task.find({user: UserId}).then((tasks) => {
    if (tasks.length === 0) {
      return res.status(404).send({"detail": `Usuario no tiene tareas`});
    }
    console.log(tasks);
    return res.send({tasks});
  }, (err) => {
    res.status(404).send(err);
  });
});

router.route('/tasks/:id')
.get(authenticate, (req, res) => {
  var id = req.params.id;
  Task.findById(id).then((task) => {
    if (!task) {
      return res.status(404).send( {"detail":`La tarea ${id} no existe`});
    }

    if (!task.isOwner(req.user._id)) {
      return res.status(403).send({"detail":"Usuario tratando de ver tarea que no le pertenece"});
    }
    res.status(200).send(task);
  }).catch( (e) => {
    res.status(400).send({"detail": `${id} no es un ObjectID`});
  });
})
.put(authenticate, (req,res) => {
  var id = req.params.id;
  console.log("Task Id: ", id);
  Task.findById(id).then((taskToUpdate) => {
    if (!taskToUpdate) {
      return res.status(404).send( {"detail":`La tarea ${id} no existe`});
    }

    //Si la tarea no le pertenece al usuario autenticado, no actualizamos
    if (!taskToUpdate.isOwner(req.user._id)) {
      return res.status(403).send({"detail":"Usuario tratando de modificar tarea que no le pertenece"});
    }

    if(taskToUpdate.completada === true) {
      return res.status(400).send({"detail":"No se pueden actualizar tareas ya completadas"});
    }

    console.log("Owner de la tarea tratando de modificarla");
    //Solo permitimos modificar estos campos
    var updates = _.pick(req.body, ['nombre','descripcion','fechaParaCompletar']);
    console.log(JSON.stringify(updates,null,4));

    //Actualizamos la tarea y la devolvemos
    Task.findByIdAndUpdate(id, {$set: updates}, {new: true, runValidators: true}).then( (task) => {
      if (!task) {
        var err_msg = {
          'detail': 'Task not found'
        };
        return res.status(404).send(err_msg);
      }
      return res.send(task);
    }).catch( (e) => {
      console.log(e);
      res.status(400).send(e);
    });

  }).catch( (e) => {
    res.status(400).send(e);
    console.log(e);
  });
})
.delete(authenticate, (req,res) => {
  var id = req.params.id;
  //Primero conseguimos la tarea para validar si existe, y si el dueño es quien quiere borrarla
  Task.findById(id).then((task) => {
    if (!task) {
      return res.status(404).send( {"detail":`La tarea ${id} no existe`});
    }

    if (!task.isOwner(req.user._id)) {
      return res.status(403).send({"detail":"Usuario tratando de eliminar tarea que no le pertenece"});
    }

    //Borramos la tarea
    Task.findByIdAndRemove(id, (err) => {
      if (err) {
        return res.status(400).send(err);
      }

      res.status(200).send({"message": `Task ${id} eliminada.`})
    });
  }).catch( (e) => {
    res.status(400).send({"detail": `${id} no es un ObjectID`});
  });
});

router.route('/tasks/:id/complete')
  .post(authenticate, (req, res) => {
    console.log("POST /complete");
    var id = req.params.id;
    Task.findById(id).then((task) => {
      if (!task) {
        return res.status(404).send( {"detail":`La tarea ${id} no existe`});
      }

      if (!task.isOwner(req.user._id)) {
        return res.status(403).send({"detail":"Usuario tratando de eliminar tarea que no le pertenece"});
      }

      if (task.completada === true) {
        return res.status(400).send({"detail":"Tarea ya completada"});
      }
      var updates = {completada: true};

      Task.findByIdAndUpdate(id, {$set: updates}, {new: true}).then( (task) => {
        if (!task) {
          var err_msg = {
            'detail': 'Task not found'
          };
          return res.status(404).send(err_msg);
        }
        return res.send(task);
      }).catch( (e) => {
        console.log(e);
        res.status(400).send(e);
      });
    }).catch( (e) => {
      res.status(400).send({"detail": `${id} no es un ObjectID`});
    });
  });

module.exports = router;
