# DoIt API Documentation
## Endpoints activos
### Host: https://api-doit.herokuapp.com/


| Accion        | Endpoint      | HTTP method|
| ------------- |:-------------:|:----------:|
| Crear Usuario          |  ``/users/register`` | POST |
|Obtener informacion de usuario|`/users/me`|GET|
| Login          |  ``/login``          | POST|
|Logout|``/logout``| DELETE|
| Crear Tarea| ``/tasks``|POST|
| Obtener Tareas| ``/tasks/me``|GET|
| Obtener Tarea | ``/tasks/:id`` | GET |
| Actualizar Tarea | ``/tasks/:id`` | PUT |
|Eliminar Tarea| ``/tasks/:id`` | DELETE |
|Completar tarea|``/tasks/:id/complete``|POST|
|Obtener Categorias|`/categories`|GET|

# Detalles y ejemplos de uso

## Usuarios
**Crear usuario**

Se debe hacer un POST a ```/users``` con un archivo que contenga los siguientes datos:
 ```
 {
	"nombre": "Name",
	"apellido": "LastName",
	"fechaDeNacimiento": "1990-10-27",
	"password": "Password",
	"formaDeRegistro": 1,
	"username": "Test"
}

 ```
 *  **Atributos**
     * **Nombre y Apellido:**  3 a 50 caracteres (solo letras)
     * **Fecha de nacimiento:** verifica que sea mayor de edad
     * **Password:** debe tener entre 7 y 50 caracteres. Lo que reciba el servidor sera encriptado
     * **Forma de registro:** Web App: 1 - Android App: 2
     * **Username:** identificador unico en el sistema, de 3 a 50 caracteres alfanumericos
     * **Activo:** booleano que indica si la cuenta esta activa o no. ``default: true``




 * **Devuelve**
     *   ```201```  si fue creado exitosamente.
     *   ```400``` no se logro crear el usuario, por falta de datos o por no cumplir las validaciones.
     * JSON con la informacion del usuario registrado. La respuesta HTTP devuelta contiene un token ``x-auth`` que debe ser usado para el manejo de tareas y datos de usuario.

**Obtener informacion de usuario**

Se debe hacer un `GET` a `/users/me` con un header X-Auth, de donde se sabra de que usuario se esta pidiendo la informacion.

## Sesion
**Login**

Se debe hacer un POST a ```/login``` con un archivo que contenga los siguientes datos:
 ```
 {
   "username": "Test",
   "password": "Password"
}

 ```
 
 * **Devuelve**
     *   `200`  si el login fue exitoso.
     *  `400` no se logro realizar el login ya sea porque no se encontró el usuario o la contraseña es incorrecta.
     * `403` si el usuario que esta tratando de hacer login esta bloqueado.
     * `429` si se paso el limite de 5 intentos fallidos de login, el cual se reinicia cada 24 horas. Bloquea al usuario en cuestion, cambiando su atributo `activo` a `false`.
      * JSON con la informacion del usuario que entro. La respuesta HTTP devuelta contiene un token ``x-auth`` que debe ser usado para el manejo de tareas y datos de usuario (El X-auth tambien se incluye en el body de la respuesta).
 
**Logout**

Se debe hacer un DELETE a ```/logout``` que tenga un header X-Auth

## Tasks

Para poder usar los endpoints de tareas se debe colocar un header ``x-auth`` en todos los requests que tenga por valor un token de autenticacion, el cual se puede obtener al registrar un usuario o hacer login (En los headers de la respuesta del servidor, en el campo ``x-auth``).

Estos tokens se utilizan para determinar que usuario esta operando con las tareas, y si esta autenticado.

Si no se provee un token` x-auth`, se devolvera un error `403`.


**Crear Tarea**

Para crear una tarea se debe hacer un ``POST`` a ``/tasks``. 

Si el cuerpo del request posee datos validos para hacer una tarea, se creará y esta nueva tarea tendra como campo `user` el `_id` del usuario obtenido por el token `x-auth`

Ejemplo de tarea:
```
{
	"nombre": "Documentar api",
	"descripcion": "Terminar de documentar el endpoint de tareas",
	"fechaParaCompletar": "2030-12-24",
	"category": "5a4e5ec3e4247fd71b2c78ba"
}
```
*  **Atributos**
    * **Nombre:**  3 a 50 caracteres. Solo permite letras y numeros
    * **Descripcion:** Permite cualquier caracter y longitud.
    * **Fecha de registro:** fecha en la que se creo la tarea, creada automaticamente por el sistema al registrar una tarea.
    * **Fecha para completar:** fecha en la que se deberia completar la tarea. Se valida que sea una fecha en el futuro.
    * **Completada:** booleano que indica si la tarea ha sido terminada. ``default: false``
    * **user**: ObjectId de usuario que agrego la tarea.
    * **category**: ObjectId de categoria a la que pertenece la tarea

* **Devuelve**
    *   ```200```  si se creo correctamente la tarea.
    *   ```401``` autenticacion de usuario fallida. (Token invalido)
    *   ```400``` error creando la tarea.
    
**Obtener todas las tareas de un usuario**

Se debe hacer un ``GET`` a ``/tasks/me``, lo que devolvera la lista de tareas del usuario a quien corresponde el token `x-auth` con un codigo `200`, o ``404`` si no tiene tareas.

**Obtener tarea**

Se debe hacer un ``GET`` a ``/tasks/:id`` donde ``:id`` seria el atributo ``_id`` de una tarea. 

**Actualizar tarea**

Se debe hacer un ``PUT`` a ``/tasks/:id`` donde ``:id`` seria el atributo ``_id`` de una tarea. 
En el cuerpo del request se deben colocar los valores de los campos que seran reemplazados. Unicamente se pueden modificar: _nombre, descripcion y fechaParaCompletar_.

Solo se podran actualizar las tareas si el `_id` del usuario obtenido por el token `x-auth` es el mismo que el campo `user` de la tarea a modificar. De no cumplirse esta condicion, se devolvera un error `403`.

Ejemplo: 
```
PUT /tasks/5a4e5ec3e2247fd71b2c78bz
{
	"nombre": "Nuevo nombre",
	"descripcion": "Nueva desrcipcion",
	"fechaParaCompletar": "2030-10-10"
}
```

**Eliminar tarea**

Se debe hacer un ``DELETE`` a ``/tasks/:id`` donde ``:id`` seria el atributo ``_id`` de una tarea.

**Completar tarea**

Se debe hacer un ``POST`` a ``/tasks/:id/complete`` donde ``:id`` seria el atributo ``_id`` de una tarea.

Ejemplo:
```
POST /tasks/5a4e5ec3e2247fd71b2c78bz/complete
```



