var express = require('express');
var bodyParser = require('body-parser');
var md5 = require('md5');
const _ = require('lodash');
var cors = require('cors');

var {mongoose} = require('./mongoose');
var userRouter = require('../routes/users');
var sessionRouter = require('../routes/session');
var taskRouter = require('../routes/tasks');
var categoryRouter = require('../routes/categories');

var app = express();
const port = process.env.PORT || 3000;

var corsOptions = {
    allowedHeaders: 'Content-Type,X-Auth,x-auth,cache-control'
}

app.use(cors(corsOptions));
app.options('*', cors(corsOptions));

app.use(bodyParser.json());
app.use(userRouter);
app.use(sessionRouter);
app.use(taskRouter);
app.use(categoryRouter);

app.listen(port, () => {
  console.log(`Started on port ${port}`);
});

module.exports = {
  app: app
};
