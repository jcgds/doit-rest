var mongoose = require('mongoose');
var validate = require('mongoose-validator');

var alphaValidator = [
  validate({
    validator: 'isLength',
    arguments: [3,50],
    message: 'Debe contener entre {ARGS[0]} y {ARGS[1]} caracteres'
  }),
  validate({
    validator: 'isAlpha',
    passIfEmpty: true,
    message: 'Solo se permiten letras'
  })
];

var fechaDeNacimientoValidator = [
  validate({
    validator: function minorValidator(value) {
      var currentYear = new Date().getFullYear();
      return (currentYear - value.getFullYear() > 17);
    },
    message: 'El usuario debe ser mayor de edad'
  })
];

//Este validator es inutil porque esta recibiendo una contrase;a ya hasheada (MD5), asi que sera alfanumerico
var passwordValidator = [
  validate({
    validator: 'isLength',
    arguments: [7,50],
    message: 'La contrase;a debe tener entre 7 y 50 caracteres'
  }),
  validate({
    validator: 'isAlphanumeric',
    message: 'La contrase;a solo admite caracteres alfanumericos'
  })
];

var usernameValidator = [
  validate({
    validator: 'isLength',
    arguments: [3,50],
    message: 'Debe tener entre 3 y 50 caracteres'
  }),
  validate({
    validator: 'isAlphanumeric',
    message: 'Solo admite caracteres alfanumericos'
  })
];

var alphaNumericValidator = [
  validate({
    validator: 'isLength',
    arguments: [3,50],
    message: 'Debe contener entre {ARGS[0]} y {ARGS[1]} caracteres'
  }),
  validate({
    validator: 'matches',
    arguments: ['^[A-Za-z0-9 ]+$'],
    message: 'Solo se permiten letras, numeros y espacios'
  })
];

var fechaParaCompletarValidator = [
  validate({
    validator: function dateValidator (value) {
      var currentTime = new Date().getTime();
      return (value.getTime() - currentTime > 0)
    },
    message: 'La fecha a completar debe ser mayor a la actual'
  })
];

var valid = {
  alphaValidator: alphaValidator,
  fechaDeNacimientoValidator: fechaDeNacimientoValidator,
  passwordValidator: passwordValidator,
  usernameValidator: usernameValidator,
  alphaNumericValidator: alphaNumericValidator,
  fechaParaCompletarValidator: fechaParaCompletarValidator
}

module.exports = {valid};
