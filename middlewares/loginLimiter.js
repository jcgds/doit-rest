const RateLimit = require('express-rate-limit');
const {User} = require('../models/user.js');

// Los keys en vez de ser la IP, son el nombre de usuario
var keyGenerator = function(req) {
  return req.body.username;
};

//Cuando se pase el limite actualizamos el usuario y lo desactivamos
var handler = function(req, res) {
  console.log(`Limit exceeded, blocking user ${req.body.username}`);
  User.findOneAndUpdate({username: req.body.username}, {activo: false}, {new: true}).then( (user, err) => {
    if (err) {
      return res.status(400).send(err);
    }
    return res.status(429).send({'error': `Login attempts limit exceeded, user ${user.username} blocked`});
  });
};

var loginLimiter = new RateLimit({
  windowMs: 24*60*60*1000, // Se reinicia el limite cada 24 horas. El usuario es bloqueado al pasar el MAX y no deberia poder logearse despues.
  max: 5, // Cada usuario puede hacer 5 login attempts cada 24 horas, si logra logearse se reinicia el limite.
  delayMs: 0,
  keyGenerator: keyGenerator,
  handler: handler
});

module.exports = {loginLimiter};
