const expect = require('expect');
const request = require('supertest');
const {app} = require('./../initializers/server');
var {Category} = require('./../models/category');

var category_data = {
  nombre: 'Prueba',
  activa: true
};

beforeEach((done) => {
  Category.remove({}).then(() => done());
});

//Prueba del modelo de Categoria
describe('Prueba modelo Categoria', () => {
  it('Deberia guardar una categoria en la base de datos', (done) => {
    var category = new Category(category_data);
    category.save( (err) => {
      if (err) {
        return done(err);
      }

      //Conseguimos la categoria en la BD para comprobar
      Category.find({_id:category._id}).then( (res) => {
        expect(res.length).toBe(1);
        done();
      }).catch( (e) => done(e));
    });
  });
});

describe('Prueba endpoint POST /categories', () => {
  it('Deberia crear una nueva categoria y devolver el codigo 201', (done) => {
    request(app)
      .post('/categories')
      .send(category_data)
      .expect(201)
      .expect( (res) => {
        expect(res.body.nombre).toBe(category_data.nombre);
      })
      .end( (err,res) => {
        if (err) return done(err);
        done();
      });
  });

  it('Al mandar solo el campo \'activa\' deberia devolver un error con codigo 400', (done) => {
    request(app)
      .post('/categories')
      .send({activa: category_data.activa})
      .expect(400)
      .end( (err,res) => {
        if (err) return done(err);
        done();
      });
  });

  it('Al mandar un nombre repetido deberia devolver codigo 400', (done) => {
    //Creamos la categoria por primera vez
    request(app)
      .post('/categories')
      .send(category_data)
      .expect(201)
      //Al terminar el primer request, hacemos el segundo con los mismos datos para forzar el codigo 400
      .end( (err,res) => {
        if (err) return done(err);

        request(app)
          .post('/categories')
          .send(category_data)
          .expect(400)
          .end( (err, res) => {
            if (err) return done(err);
            done();
          });
      });
  });
});

describe('Prueba GET /categories', () => {
  it('Deberia devolver todas las categorias en la base de datos', (done) => {
    //Primero creamos una categoria
    request(app)
      .post('/categories')
      .send(category_data)
      .expect(201)
      .end( (err,res) => {
        if (err) return done(err);

        request(app)
          .get('/categories')
          .expect(200)
          .expect( (res) => {
            expect(res.body.categories.length).toBe(1);
          })
          .end( (err,res) => {
            if (err) return done(err);
            done();
          });
      });
  });
});
