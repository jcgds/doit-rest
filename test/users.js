const expect = require('expect')
const request = require('supertest')
const _ = require('lodash');

const {app} = require('./../initializers/server')
const {User} = require('./../models/user')

beforeEach((done) => {
  User.remove({}).then(() => done());
});

var user = {
  "nombre": "Juan",
  "apellido": "Goncalves",
  "fechaDeNacimiento": "1997-10-17",
  "password": "Password",
  "formaDeRegistro": 0,
  "username": "Testerino"
}

describe('POST /users/register', () => {
  it('Deberia crear un nuevo usuario', (done) => {
    request(app)
      .post('/users/register')
      .send(user)
      .expect(201)
      .expect((res) => {
        expect(res.body.username).toBe(user.username.toLowerCase());
      })
      .end( (err, res) => {
        if (err) {
          return done(err);
        }

        User.find({username: user.username}).then((res) => {
          done();
        }).catch( (e) => done(e));
      });
  });
});

describe('GET /users/me', () => {
  actUser = User(user);
  it('Deberia recibir la informacion del usuario dueño del token',
      (done) => {
        actUser.generateAuthToken().then( (token) => {
          request(app)
          .get('/users/me')
          .set('x-auth', token)
          .send()
          .expect(200)
          .expect( (res) => {
            expect(res.body.username).toBe(actUser.username);
          })
          .end( (err,res) => {
            if (err) {
              return done(err);
            }
            done();
          });
        });
    });

    it('Deberia devolver un error 403 al tratar de recibir informacion de un usuario sin autenticacion', (done) => {
      request(app)
        .get('/users/me')
        .send()
        .expect(401)
        .end( (err,res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
});
