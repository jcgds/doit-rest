const expect = require('expect');
const request = require('supertest');
const {app} = require('./../initializers/server');
const {Task} = require('./../models/task');
const {Category} = require('./../models/category');
const {User} = require('./../models/user')

const category = new Category({nombre: 'Pruebas', activa: true});

const user = new User({
  "nombre": "Juan",
  "apellido": "Goncalves",
  "fechaDeNacimiento": "1997-10-17",
  "password": "Password",
  "formaDeRegistro": 0,
  "username": "Testerino"
});

var task_data = {
  nombre: 'Prueba task',
  descripcion: 'Test del modelo Task',
  fechaParaCompletar: '3000-12-31',
  category: category._id,
  user: user._id
}

beforeEach((done) => {
  Task.remove({}).then(() => done());
});

describe('Pruebas POST /tasks', () => {
  it('Al pasar un token x-auth y datos de tarea validos, se deberia crear la tarea y devolver 201', (done) => {
    user.generateAuthToken().then( (token) => {
      category.save( (err) => {
        if (err) {
          return done(err);
        }
        request(app)
          .post('/tasks')
          .set('x-auth', token)
          .send(task_data)
          .expect(201)
          .end( (err,res) => {
            if (err) return done(err);
            done();
          });
      });
    });
  });

  it('Si no se envia un x-auth deberia devolver 401', (done) => {
    request(app)
      .post('/tasks')
      .send(task_data)
      .expect(401)
      .end( (err,res) => {
        if (err) return done(err);
        done();
      });
  });
});
