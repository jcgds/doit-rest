const expect = require('expect');
const request = require('supertest');
const {app} = require('./../initializers/server');
const {User} = require('./../models/user');

beforeEach((done) => {
  User.remove({}).then(() => done());
});

var user_data = {
  username: 'testerino',
  password: 'password',
  nombre: 'Prueba',
  apellido: 'login',
  formaDeRegistro: 0,
  fechaDeNacimiento: '1997-10-17'
};

var user = new User(user_data);

describe('Pruebas POST /login', () => {
  it('Al mandar datos validos, deberia devolver codigo 200', (done) => {
    var login_data = {
      username: user.username,
      password: user.password
    };
    user.save( (err) => {
      if (err)  return done(err);

      request(app)
        .post('/login')
        .send(login_data)
        .expect(200)
        .end( (err, res) => {
          if (err) return done(err);
          done();
        });
    });
  });

  it('Al mandar datos invalidos, deberia devolver codigo 400', (done) => {
    var login_data = {
      username: 'wrong',
      password: user.password
    };
    user.save( (err) => {
      if (err)  return done(err);

      request(app)
        .post('/login')
        .send(login_data)
        .expect(400)
        .end( (err, res) => {
          if (err) return done(err);
          done();
        });
    });
  });
});
