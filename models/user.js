var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var uniqueValidator = require('mongoose-unique-validator');
var md5 = require('md5');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
var {valid} = require('../middlewares/validators');

var userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true,
    validate: valid.usernameValidator
  },
  password: {
    type: String,
    required: true,
    validate: valid.passwordValidator
  },
  nombre: {
    type: String,
    required: true,
    trim: true,
    validate: valid.alphaValidator
  },
  apellido: {
    type: String,
    required: true,
    trim: true,
    validate: valid.alphaValidator
  },
  fechaDeNacimiento: {
    //YYYY-MM-DD
    type: Date,
    required: true,
    validate: valid.fechaDeNacimientoValidator
  },
  formaDeRegistro: {
    /*
    *  0 - Request directo
    *  1 - Web app
    *  2 - Android app
    */
    type: Number,
    required: true
  },
  fechaDeRegistro: {
    type: Date,
    default: Date.now
  },
  activo: {
    type: Boolean,
    default: true
  },
  salt: {
    type: String
  },
  tokens: [{
    access:{
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
},
//Esto permite pushear a MongoDB los array sin errores
{usePushEach: true}
);

userSchema.plugin(uniqueValidator);

userSchema.methods.toJSON = function(){
  var user = this;
  var userObject = user.toObject();

  return _.pick(userObject, ['_id','username','nombre','apellido',
'fechaDeNacimiento','formaDeRegistro','fechaDeRegistro','activo']);
};

userSchema.methods.generateAuthToken = function() {
  var user = this;
  var access = 'auth';
  //Recorar cambiar el secret 123456 del token
  var token = jwt.sign({_id: user._id.toHexString(), access}, '123456').toString();

  user.tokens.push({access, token});

  return user.save().then(() => {
    return token;
  }).catch( (e) => {
    console.log(e);
  });
};

userSchema.methods.removeToken = function (token) {
  var user = this;

  return user.update({
    $pull: {
      tokens: {token}
    }
  })
};

userSchema.statics.findByToken = function (token) {
  var User = this;
  var decoded;

  try {
    decoded = jwt.verify(token, '123456');
  } catch (e) {
    return Promise.reject();
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
};

//Esto se corre antes de guardar el usuario a la base de datos (.save())
userSchema.pre('save', function(next){
  var user = this;
  //Verificamos que se haya cambiado la password, porque si no, estariamos volviendo a hashearla
  if (user.isModified('password')) {
    bcrypt.genSalt(10, (err, salt) => {
        user.salt = salt;
        var hashedPassword = md5(user.password + user.salt);
        user.password = hashedPassword;
        next();
    });
  } else {
    next();
  }
});

userSchema.methods.validPassword = function (nonHashedPassword) {
  var user = this;
  var hashIt = md5(nonHashedPassword + user.salt);
  return hashIt.valueOf() === user.password;
};

userSchema.statics.findByCredentials = function (username, password){
  var user = this;

  return user.findOne({username}).then((user) => {
    if(!user){
      console.log("User no encontrado");
      return Promise.reject();
    } else {
      return new Promise((resolve, reject) => {
        if (user.validPassword(password)){
          resolve(user);
        } else {
          console.log("Contraseña invalida");
          reject();
        }
      });
    }
  });
};

var User = mongoose.model('User',userSchema);

module.exports = {
  User: User
};
