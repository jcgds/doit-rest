var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var uniqueValidator = require('mongoose-unique-validator');
const _ = require('lodash');
var ObjectId = mongoose.Schema.ObjectId;
const {ObjectID} = require('mongodb');
var {valid} = require('../middlewares/validators');

var taskSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
    validate: valid.alphaNumericValidator
  },
  descripcion: {
    type: String,
    trim: true,
  },
  completada: {
    type: Boolean,
    default: false
  },
  fechaDeRegistro: {
    type: Date,
    default: Date.now
  },
  fechaParaCompletar: {
    //YYYY-MM-DD
    type: Date,
    validate: valid.fechaParaCompletarValidator
  },
  user: {
    type: ObjectId,
    required: true
  },
  category: {
    type: ObjectId,
    required: true
  }
});

taskSchema.plugin(uniqueValidator);

taskSchema.methods.toJSON = function(){
  var task = this;
  var taskObject = task.toObject();

  return taskObject;
};

taskSchema.methods.isOwner = function(userId) {
  var task = this;
  var authenticatedUser = new ObjectID(userId);
  if(!task.user.equals(authenticatedUser)) return false;

  return true;
}

var Task = mongoose.model('Task', taskSchema);

module.exports = {
  Task: Task
};
