var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var uniqueValidator = require('mongoose-unique-validator');
var {valid} = require('../middlewares/validators');

var categorySchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    validate: valid.alphaNumericValidator
  },
  activa: {
    type: Boolean,
    default: false
  }
});

categorySchema.plugin(uniqueValidator);

var Category = mongoose.model('Category', categorySchema);

module.exports = {Category};
